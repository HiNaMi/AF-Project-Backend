    // var express  = require('express');
    // var path = require('path');
    // var app      = express();                               // create our app w/ express
    // var mongoose = require('mongoose');                     // mongoose for mongodb
    // var morgan = require('morgan');             // log requests to the console (express4)
    // var bodyParser = require('body-parser');    // pull information from HTML POST (express4)
    // var methodOverride = require('method-override'); // simulate DELETE and PUT (express4)
    //
    //
    // // configuration =================
    //
    // mongoose.connect('mongodb://localhost:27017/af_database');     // connect to mongoDB database on modulus.io
    //
    // //app.use(express.static(__dirname + '/public_html'));                 // set the static files location /public/img will be /img for users
    // app.use(express.static(__dirname + '/public_html', { index: 'index.html' }));
    // app.use(morgan('dev'));                                         // log every request to the console
    // app.use(bodyParser.urlencoded({'extended':'true'}));            // parse application/x-www-form-urlencoded
    // app.use(bodyParser.json());                                     // parse application/json
    // app.use(bodyParser.json({ type: 'application/vnd.api+json' })); // parse application/vnd.api+json as json
    // app.use(methodOverride());
    // //app.use('/', express.static('/public_html'));
    //
    // require('./app/routes.js')(app)
    // require('./app/patient.routes.js')(app)
    // require('./app/supplier.routes.js')(app)
    // require('./app/bill.routes')(app)
    //
    //
    // // listen (start app with node server.js) ======================================
    // app.listen(8080);
    // console.log("App listening on port 8080");

'use strict';

const bodyParser = require('body-parser'),
    express = require('express'),
    mongoose = require('mongoose');

mongoose.Promise = global.Promise;

require('./src/models/patients');
require('./src/models/bill');
require('./src/models/drugdespence');
require('./src/models/notification');
require('./src/models/sendrequest');
require('./src/models/viewrequest');
// require('./comment.model');

const PatientRouter = require('./src/routes/patient');
const BillRouter = require('./src/routes/bill');
const DrugDespenceRouter = require('./src/routes/drugdespence');
const NotificationRouter = require('./src/routes/notification');
const SendRequestRouter = require('./src/routes/sendrequest');
const viewRequestRouter = require('./src/routes/viewrequest');

const app = express();

app.use(bodyParser.json());

mongoose.connect('mongodb://localhost:27017/af_database', err => {
    if (err) {
        console.log(err);
        process.exit(1);
    }
});

// app.use('/app', express.static(__dirname + '/public'));
// app.use('/app/modules', express.static(__dirname + '/bower_components'));

// app.get('/', (req, res, next) => {
//     res.sendFile(__dirname + '/public/index.html');
// });

app.use(function (req, res, next) {
    res.header("Access-Control-Allow-Origin", "*");
    res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
    next();
});

app.use('/patients', PatientRouter);
app.use('/generatebills', BillRouter);
app.use('/drugdespences', DrugDespenceRouter);
app.use('/notifications', NotificationRouter);
app.use('/sendrequests', SendRequestRouter);
app.use('/viewrequests', viewRequestRouter);
// app.get('/app/*', (req, res, next) => {
//     res.sendFile(__dirname + '/public/index.html');
// });

app.listen(3000, err => {
    if (err) {
        console.error(err);
        return;
    }
    console.log('app listening on port 3000');
});
