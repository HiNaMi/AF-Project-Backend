/*var Supplier = require('./models/supplier');

var path = require('path');

function getSuppliers(res) {
    Supplier.find(function (err, suppliers) {

        // if there is an error retrieving, send the error. nothing after res.send(err) will execute
        if (err) {
            res.send(err);
        }

        res.json(suppliers); // return all todos in JSON format
    });
};

module.exports = function (app) {

    // api ---------------------------------------------------------------------
    // get all todos
    app.get('/api/suppliers', function (req, res) {
        // use mongoose to get all todos in the database
        getSuppliers(res);
    });

    // create todo and send back all todos after creation
    app.post('/api/suppliers', function (req, res) {

        // create a todo, information comes from AJAX request from Angular
        try {
            Supplier.create({
                supplierID: Date.now() / 1000 | 0,
                name: req.body.name,
                type: req.body.type,
                email: req.body.email,
                cNumber: req.body.cNumber,
                remark: req.body.remark,
                //quantity: req.body.quantity,
                done: false
            }, function (err, supplier) {
                if (err){
                    console.log(err);
                    res.send(err);
                }
                // get and return all the todos after you create another
                getSuppliers(res);
            });
        } catch(e){
            console.log(e);
        }

    });

    // delete a todo
    app.delete('/api/suppliers/:supplier_id', function (req, res) {
        Supplier.remove({
            _id: req.params.supplier_id
        }, function (err, supplier) {
            if (err)
                res.send(err);

            getSuppliers(res);
        });
    });


};*/

'use strict';

const express = require('express'),
    mongoose = require('mongoose');

// mongoose.set('debug', false);

const SupplierModel = mongoose.model('Supplier');
    // CommentModel = mongoose.model('Comment');

const Router = express.Router();

//Get All supplier details
Router.get('/', (req, res)=>{
  SupplierModel.find().then(supplier=>{
    res.json(supplier);
  }).catch(err=>{
    console.error(err);
    res.sendStatus(500);
  });
});

//Get supplier by ID
Router.get('/:id',(req,res)=>{
  SupplierModel.findById(req.params.id).then(supplier=>{
    res.json(supplier || {});
  }).catch(err=>{
    console.error(err);
    res.sendStatus(500);
  });
});

//Add suppliers
Router.post('/supplier', (req, res) => {
    const supplier = new SupplierModel(req.body);
    supplier.save().then(supplier => {
        res.json(supplier);
    }).catch(err => {
        console.error(err);
        res.sendStatus(500);
    });
});




// Router.put('/:id', (req,res)=>{
//   const supplier = req.body;
//   delete supplier._id;
//   const supplierId = req.params.id;
//   SupplierModel.findByIdAndUpdate(supplierId, {$set:supplier}).then(af_database=>{
//     res.json(supplier);
//   }).catch(err=>{
//     console.error(err);
//     res.sendStatus(500);  //Internal error
//   });
// });

//Update supplier by id
Router.post('/updatesupplier', (req, res,next) => {

    SupplierModel.findOne({ supplierID:req.body.supplierID }).then((foundsupplier)=>{
      if(foundsupplier){
        foundsupplier.name=req.body.name;
        foundsupplier.type=req.body.type;
        foundsupplier.email=req.body.email;
        foundsupplier.cNumber=req.body.cNumber;
        foundsupplier.remark=req.body.remark;


        foundsupplier.save().then((savedsupplier)=>{
          const returnsupplierobj={
            success:'',
            message:'',
            data:{}
          };
          returnsupplierobj.success='true';
          returnsupplierobj.message='Supplier Details Updated Successfully';
          returnsupplierobj.data=savedsupplier;
          return res.json(returnsupplierobj);
        }).error((e)=>next(e));

      }else{
        const returnsupplierobj={
          success:'',
          message:'',
          data:{}
        };
        returnsupplierobj.success='false';
        returnsupplierobj.message='supplier Details didnt Updated Successfully';
        return res.json(returnsupplierobj);
      }
    });


});


/*
//delete supplier by id
Router.delete('/:id', (req,res)=>{
  SupplierModel.findByIdAndRemove(req.params.id).then((supplier=>{
    const supplierId = supplier.supplierID.map();
  }))
})
*/
//delete supplier
Router.get('/getbatch', (req, res) => {
      SupplierModel.find().then(batchs => {
          res.json(batchs);
      }).catch(err => {
          console.error(err);
          res.sendStatus(500);
      });
  });



module.exports = Router;
