/*var Drug = require('./models/drug');

var path = require('path');

function getDrugs(res) {
    Drug.find(function (err, drugs) {

        // if there is an error retrieving, send the error. nothing after res.send(err) will execute
        if (err) {
            res.send(err);
        }

        res.json(drugs); // return all todos in JSON format
    });
};


module.exports = function (app) {
 // api ---------------------------------------------------------------------
    // get all todos
    app.get('/api/drugs', function (req, res) {
        // use mongoose to get all todos in the database
        getDrugs(res);
    });

    // create todo and send back all todos after creation
    app.post('/api/drugs', function (req, res) {

        // create a todo, information comes from AJAX request from Angular
		try {
			Drug.create({
				drugID: Date.now() / 1000 | 0,
				name: req.body.name,
				type: req.body.type,
				category: req.body.category,
				unitPrice: req.body.unitPrice,
				reOrderLevel: req.body.reOrderLevel,
				dangerLevel: req.body.dangerLevel,
				Remarks: req.body.Remarks,
				quantity: req.body.quantity,
				done: false
			}, function (err, drug) {
				if (err){
					console.log(err);
					res.send(err);
				}
				// get and return all the todos after you create another
				getDrugs(res);
			});
		} catch(e){
			console.log(e);
		}

    });

    // delete a todo
    app.delete('/api/drugs/:drug_id', function (req, res) {
        Drug.remove({
            _id: req.params.drug_id
        }, function (err, drug) {
            if (err)
                res.send(err);

            getDrugs(res);
        });
    });
};
	*/

'use strict';

const express = require('express'),
    mongoose = require('mongoose');

// mongoose.set('debug', false);

const DrugModel = mongoose.model('Drug');
// CommentModel = mongoose.model('Comment');

const DrugRouter = express.Router();

DrugRouter.post('/adddrugs', (req, res) => {
    const drug = new DrugModel(req.body);
    drug.save().then(drug => {
        res.json(drug);
    }).catch(err => {
        console.error(err);
        res.sendStatus(500);
    });
});

DrugRouter.get('/', (req, res) => {
    DrugModel.find().then(drugs => {
        res.json(drugs);
    }).catch(err => {
        console.error(err);
        res.sendStatus(500);
    });
});

DrugRouter.get('/getStatus', (req, res) => {
    DrugModel.find({status:true}).then(drugs => {
        res.json(drugs);
    }).catch(err => {
        console.error(err);
        res.sendStatus(500);
    });
});

/*DrugRouter.put('/:drugid', (req, res) => {
    const drug = req.body;
    delete drug._id;
    const drugId = req.params.id;
    DrugModel.findByIdAndUpdate(drugId, {$set: drug}).then(drugDb => {
        res.json(drug);
    }).catch(err => {
        console.error(err);
        res.sendStatus(500);
    });
});*/

DrugRouter.post('/updatedrugs', (req, res,next) => {
  //  const drug = new DrugModel(req.body);
    //drug.save().then(drug => {
    //    res.json(drug);
    //}).catch(err => {
    //    console.error(err);
    //    res.sendStatus(500);
  //  }

    DrugModel.findOne({ drugID:req.body.drugID }).then((founddrug)=>{
      if(founddrug){
        founddrug.name=req.body.name;
        founddrug.type=req.body.type;
        founddrug.category=req.body.category;
        founddrug.unitPrice=req.body.unitPrice;
        founddrug.reOrderLevel=req.body.reOrderLevel;
        founddrug.dangerLevel=req.body.dangerLevel;
        founddrug.remarks=req.body.remarks;
        founddrug.quantity=req.body.quantity;

        founddrug.save().then((saveddrug)=>{
          const returndrugobj={
            success:'',
            message:'',
            data:{}
          };
          returndrugobj.success='true';
          returndrugobj.message='Drug Details Updated Successfully';
          returndrugobj.data=saveddrug;
          return res.json(returndrugobj);
        }).error((e)=>next(e));

      }else{
        const returndrugobj={
          success:'',
          message:'',
          data:{}
        };
        returndrugobj.success='false';
        returndrugobj.message='Drug Details didnt Updated Successfully';
        // returndrugobj.data=saveddrug;
        return res.json(returndrugobj);
      }
    });


});








module.exports = DrugRouter;
