// var Patients = require('./models/patients');
//
// var path = require('path');
//
// function getPatients(res) {
//     Patients.find(function (err, patient) {

        // if there is an error retrieving, send the error. nothing after res.send(err) will execute
//         if (err) {
//             res.send(err);
//         }
//
//         res.json(patient); // return all todos in JSON format
//     });
// };
//
// module.exports = function (app) {

    // api ---------------------------------------------------------------------
    // get all todos
    // app.get('/api/patient', function (req, res) {
    //     // use mongoose to get all todos in the database
    //     getPatients(res);
    // });

    // createtodo and send back all todos after creation
//     app.post('/api/patient', function (req, res) {
//
//         // create atodo, information comes from AJAX request from Angular
//         try {
//             Patients.create({
//                 patientID: Date.now() / 1000 | 0,
//                 title: req.body.title,
//                 firstname: req.body.firstname,
//                 lastname: req.body.lastname,
//                 dateofbirth: req.body.dateofbirth,
//                 gender: req.body.gender,
//                 NIC: req.body.NIC,
//                 address: req.body.address,
//                 phoneNo: req.body.phoneNo,
//                 done: false
//             }, function (err, patient) {
//                 if (err){
//                     console.log(err);
//                     res.send(err);
//                 }
//                 // get and return all the todos after you create another
//                 getPatients(res);
//             });
//         } catch(e){
//             console.log(e);
//         }
//
//     });
//
//     // delete atodo
//     app.delete('/api/patient/:patient_id', function (req, res) {
//         Patients.remove({
//             _id: req.params.patient_id
//         }, function (err, patient) {
//             if (err)
//                 res.send(err);
//
//             getPatients(res);
//         });
//     });
//
//
// };

'use strict';

const express = require('express'),
    mongoose = require('mongoose');

// mongoose.set('debug', false);

const PatientModel = mongoose.model('Patient');
    // CommentModel = mongoose.model('Comment');

const Router = express.Router();

Router.get('/', (req, res) => {
    PatientModel.find().then(patients => {
        res.json(patients);
    }).catch(err => {
        console.error(err);
        res.sendStatus(500);
    });
});
//
// Router.get('/:id', (req, res) => {
//     DriverModel.findById(req.params.id).populate('comments').exec().then(driver => {
//         res.json(driver || {});
//     }).catch(err => {
//         console.error(err);
//         res.sendStatus(500);
//     });
// });

Router.post('/create', (req, res) => {
    const patient = new PatientModel(req.body);
    patient.save({$inc: { patientID: 1} }).then(patient => {
        res.json(patient);
    }).catch(err => {
        console.error(err);
        res.sendStatus(500);
    });
});
//
// Router.put('/:id', (req, res) => {
//     const driver = req.body;
//     delete driver._id;
//     const driverId = req.params.id;
//     DriverModel.findByIdAndUpdate(driverId, {$set: driver}).then(driverDb => {
//         res.json(driver);
//     }).catch(err =>
//         console.error(err);
//     res.sendStatus(500);
// });
// });
//
// Router.delete('/:id', (req, res) => {
//     DriverModel.findByIdAndRemove(req.params.id).then((driver) => {
//         const commentIds = driver.comments.map((commentId => commentId));
//         return CommentModel.remove({_id: {$in: commentIds}});
//     }).then(() => {
//         res.sendStatus(200);
//     }).catch(err => {
//         console.error(err);
//         res.sendStatus(500);
//     });
// });
//
// Router.post('/:id/comments', (req, res) => {
//     let comment = new CommentModel(req.body);
//     const driverId = req.params.id;
//     comment.driver = driverId;
//     comment.save().then(commentDb => {
//         return DriverModel.findByIdAndUpdate(driverId, {$push: {"comments": commentDb._id}})
//     }).then(() => {
//         return DriverModel.findById(driverId).populate('comments').exec();
//     }).then(driverDb => {
//         res.json(driverDb);
//     }).catch(err => {
//         console.error(err);
//         res.sendStatus(500);
//     });
// });

module.exports = Router;
