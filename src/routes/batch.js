'use strict'

const express = require('express'),
    mongoose = require('mongoose');

// mongoose.set('debug', false);

const BatchModel = mongoose.model('Batch');
//const DrugModel = mongoose.model('Drug');
// CommentModel = mongoose.model('Comment');

const BatchRouter = express.Router();
//const DrugRouter=express.Router();

BatchRouter.post('/addbatchs', (req, res) => {
    const batch = new BatchModel(req.body);
    batch.save().then(batch => {
        res.json(batch);
    }).catch(err => {
        console.error(err);
        res.sendStatus(500);
    });
});

/*DrugRouter.get('/', (req, res) => {
    DrugModel.find().then(drugs => {
        res.json(drugs);
    }).catch(err => {
        console.error(err);
        res.sendStatus(500);
    });
});*/

BatchRouter.get('/getbatch', (req, res) => {
    BatchModel.find().then(batchs => {
        res.json(batchs);
    }).catch(err => {
        console.error(err);
        res.sendStatus(500);
    });
});

BatchRouter.post('/updatebatches', (req, res,next) => {
  //  const drug = new DrugModel(req.body);
    //drug.save().then(drug => {
    //    res.json(drug);
    //}).catch(err => {
    //    console.error(err);
    //    res.sendStatus(500);
  //  }

    BatchModel.findOne({ batchNo:req.body.batchNo }).then((foundBatch)=>{
      if(foundBatch){
        foundBatch.quantity=req.body.quantity;
        foundBatch.menuDate=req.body.menuDate;
        foundBatch.expiryDate=req.body.expiryDate;
        foundBatch.Type=req.body.Type;
        foundBatch.content=req.body.content;
        foundBatch.Bottleliquid.NumberofBottle=req.body.Bottleliquid.NumberofBottle;
        foundBatch.BottleTablets.NumberofBottletablet=req.body.BottleTablets.NumberofBottletablet;
        foundBatch.cartoonsLiquid.NumberofCartoons=req.body.cartoonsLiquid.NumberofCartoons;
        foundBatch.cartoonsTablets.cartoonsTabletsBottles.NumberofBottlecartoonsTab=req.body.cartoonsTablets.cartoonsTabletsBottles.NumberofBottlecartoonsTab;
        foundBatch.cartoonsTablets.cartoonsTabletsBottles.NumberoftabletsperBottles=req.body.cartoonsTablets.cartoonsTabletsBottles.NumberoftabletsperBottles;
        foundBatch.cartoonsTablets.cartoonsTabletsCards.NumberofCartoonsCards=req.body.cartoonsTablets.cartoonsTabletsCards.NumberofCartoonsCards;
        foundBatch.cartoonsTablets.cartoonsTabletsCards.NumberofCardspercartoons=req.body.cartoonsTablets.cartoonsTabletsCards.NumberofCardspercartoons;
        foundBatch.cartoonsTablets.cartoonsTabletsCards.NumberofTabletsperCard=req.body.cartoonsTablets.cartoonsTabletsCards.NumberofTabletsperCard;
        foundBatch.category=req.body.category;
        foundBatch.name=req.body.name;
        foundBatch.batchStatus=req.body.batchStatus;
        foundBatch.changeBatchstatus=req.body.changeBatchstatus;



        foundBatch.save().then((saveBatch)=>{
          const returndrugobj={
            success:'',
            message:'',
            data:{}
          };
          returndrugobj.success='true';
          returndrugobj.message='Batch Details Updated Successfully';
          returndrugobj.data=saveBatch;
          return res.json(returndrugobj);
        }).error((e)=>next(e));

      }else{
        const returndrugobj={
          success:'',
          message:'',
          data:{}
        };
        returndrugobj.success='false';
        returndrugobj.message='Batch Details didnt Updated Successfully';
        // returndrugobj.data=saveddrug;
        return res.json(returndrugobj);
      }
    });
  });

  BatchRouter.delete('/:_id', (req, res) => {
      BatchModel.findByIdAndRemove(req.params._id).then((driver) => {
          res.sendStatus(200);
      }).catch(err => {
          console.error(err);
          res.sendStatus(500);
      });
  });

  /*BatchRouter.post('/addbatchs', (req, res) => {
      const batch = new BatchModel(req.body);
      batch.find(req.body._id).then((foundExpiredDrug) => {
        if()
          res.json(batch);
      }).catch(err => {
          console.error(err);
          res.sendStatus(500);
      });
  });*/



module.exports = BatchRouter;
//module.exports = DrugRouter;
