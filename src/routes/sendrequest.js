'use strict';

const express = require('express'),
    mongoose = require('mongoose');

const SendRequestModel = mongoose.model('SendRequest');

const Router = express.Router();

Router.post('/sendrequest', (req, res) => {
    const sendrequest = new SendRequestModel(req.body);
    sendrequest.save().then(sendrequest => {
        res.json(sendrequest);
    }).catch(err => {
        console.error(err);
        res.sendStatus(500);
    });
});

module.exports = Router;
