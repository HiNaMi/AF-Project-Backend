'use strict';

const express = require('express'),
    mongoose = require('mongoose');

const DrugDespenceModel = mongoose.model('DrugDespence');

const Router = express.Router();

Router.get('/despence', (req, res) => {
    DrugDespenceModel.find().then(despence => {
        res.json(despence);
    }).catch(err => {
        console.error(err);
        res.sendStatus(500);
    });
});

//Router.post('/despence', (req, res) => {
//    const despence = new DrugDespenceModel(req.body);
//    despence.save().then(despence => {
//        res.json(despence);
//    }).catch(err => {
//        console.error(err);
//        res.sendStatus(500);
//    });
//});

module.exports = Router;
