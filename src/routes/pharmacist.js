'use strict';

const express = require('express'),
    mongoose = require('mongoose');

// mongoose.set('debug', false);

const PharmacistModel = mongoose.model('Pharmacist');
    // CommentModel = mongoose.model('Comment');

const Router = express.Router();

//Get All Pharmacist details
Router.get('/', (req, res)=>{
  PharmacistModel.find().then(pharmacist=>{
    res.json(pharmacist);
  }).catch(err=>{
    console.error(err);
    res.sendStatus(500);
  });
});


//Add Pharmacists
Router.post('/pharma', (req, res) => {
    const pharmacist = new PharmacistModel(req.body);
    pharmacist.save().then(pharmacist => {
        res.json(pharmacist);
    }).catch(err => {
        console.error(err);
        res.sendStatus(500);
    });
});

//Update supplier by id
Router.post('/updatepharm', (req, res,next) => {

    PharmacistModel.findOne({ PharmID:req.body.PharmID }).then((foundpharma)=>{
      if(foundpharma){
        foundpharma.name=req.body.name;
        foundpharma.nic=req.body.nic;
        foundpharma.gender=req.body.gender;
        foundpharma.email=req.body.email;
        foundpharma.cNumber=req.body.cNumber;
        foundpharma.address=req.body.address;
        foundpharma.password=req.body.password;


        foundpharma.save().then((savedpharma)=>{
          const returnpharmaobj={
            success:'',
            message:'',
            data:{}
          };
          returnpharmaobj.success='true';
          returnpharmaobj.message='Pharmacist Details Updated Successfully';
          returnpharmaobj.data=savedpharma;
          return res.json(returnpharmaobj);
        }).error((e)=>next(e));

      }else{
        const returnpharmaobj={
          success:'',
          message:'',
          data:{}
        };
        returnpharmaobj.success='false';
        returnpharmaobj.message='Pharmacist Details didnt Updated Successfully';
        return res.json(returnpharmaobj);
      }
    });


});

Router.get('/usercheck', function(req, res) {
    PharmacistModel.findOne({PharmID: req.query.PharmID}, function(err, user){
        if(err) {
          console.log(err);
        }
        const message = "";
        if(user) {
          console.log(user)
            message = "User exists";
            console.log(message)
        } else {
            message= "User doesn't exist";
            console.log(message)
        }
        res.json({message: message});
    });
});


//Above method and below method is working but even there is a user , it says status as failed
Router.post('/login', function(req, res)
{
  PharmacistModel.findOne({PharmID: req.query.PharmID}, function(err, users) {

  if( !err && users && password === req.query.password ) {

        res.send({ status: 'success' });
        }
  else {
    res.send({ status: 'failed' });
        }

  });
});



module.exports = Router;
