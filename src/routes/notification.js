'use strict';

const express = require('express'),
    mongoose = require('mongoose');

const NotificationModel = mongoose.model('Notification');

const Router = express.Router();

Router.get('/notification', (req, res) => {
    NotificationModel.find().then(notification => {
        res.json(notification);
    }).catch(err => {
        console.error(err);
        res.sendStatus(500);
    });
});

module.exports = Router;
