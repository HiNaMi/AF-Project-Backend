'use strict';

const express = require('express'),
    mongoose = require('mongoose'),
    pdf = require('pdfkit'),
    fs = require('fs');

const BillModel = mongoose.model('Bill');

const Router = express.Router();

//cerate new bill for out side customers
Router.post('/generatebill', (req, res) => {
    const bill = new BillModel(req.body);
    bill.save().then(bill => {
        res.json(bill);
    }).catch(err => {
        console.error(err);
        res.sendStatus(500);
    });
});

//print bill as pdf
var printBill = new pdf;

printBill.pipe(fs.createWriteStream('bill.pdf'));

printBill.font('Times-Roman').fontSize(16).text('Bill');

printBill.end();

module.exports = Router;
