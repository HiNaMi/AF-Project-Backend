'use strict';

const express = require('express'),
    mongoose = require('mongoose'),
    send = require('sendrequest');

const viewRequestModel = mongoose.model('viewRequest');

const Router = express.Router();

Router.get('/view', (req, res) => {
    viewRequestModel.find().then(view => {
        res.json(view);
    }).catch(err => {
        console.error(err);
        res.sendStatus(500);
    });
});

module.exports = Router;
