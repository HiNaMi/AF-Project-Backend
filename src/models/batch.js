/**
 * Created by kushali on 6/28/2017.
 */
'use strict'

var mongoose = require('mongoose');
const Schema = mongoose.Schema;

const BatchSchema = new Schema({
    batchNo: {
        type: String,
        unique: true,
        required: true
    },
    quantity: {
        type: Number,
        required: true
    },
    menuDate: {
        type: Date,
        required: true
    },
    expiryDate: {
        type: Date,
        required: true
    },
    Type:{
      type:String,
      required:true
    },
    content:{
      type:String,
      required:true
    },
    Bottleliquid:{
      NumberofBottle:{
        type:Number,
        required:true
      }
    },
    BottleTablets:{
      NumberofBottletablet:{
        type:Number,
        required:true
      },
      TabletsperBottles:{
        type:Number,
        required:true
      }
    },
    cartoonsLiquid:{
      NumberofCartoons:{
        type:Number,
        required:true
      },
      NumberofBottlepercartoon:{
        type:Number,
        required:true
      }
    },
    cartoonsTablets:{
      cartoonsTabletsBottles:{
        NumberofBottlecartoonsTab:{
          type:Number,
          required:true
        },
        NumberoftabletsperBottles:{
          type:Number,
          required:true
        }
      },
      cartoonsTabletsCards:{
        NumberofCartoonsCards:{
          type:Number,
          required:true
        },
        NumberofCardspercartoons:{
          type:Number,
          required:true
        },
        NumberofTabletsperCard:{
          type:Number,
          required:true
        }
      }
    },
    category:{
      type:String,
      required:true
    },
    name:{
      type:String,
      required:true
    },
    batchStatus:{
      type:Boolean,
      required:true
    },
    changeBatchstatus:{
      type:Boolean,
      required:true
    }


});

const Batch = mongoose.model('Batch', BatchSchema);

module.exports = Batch;
