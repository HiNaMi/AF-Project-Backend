//var mongoose = require('mongoose');

//module.exports = mongoose.model('Drug', {
//    drugID: {
//        type: String,
//        unique: true,
//        required: true
//    },
//    name: {
//        type: String,
//        required: true
//    },
//    type: {
//        type: String,
//        required: true
//    },
//    category: {
//        type: Date,
//        required: true
//    },
//    unitPrice: {
//        type: Number,
//        required: true
//    },
//    reOrderLevel: {
//        type: Number,
//        required: true
//    },
//    dangerLevel: {
//        type: Number,
//        required: true
//    },
//    remarks: {
//        type: String,
//        required: true
//    },
//    quantity: {
//        type: Number,
//        required: true
//    }
//})
'use strict'

const mongoose = require('mongoose');

const Schema = mongoose.Schema;

const DrugSchema = new Schema({
    drugID: {
        type: String,
        unique: true,
        required: true
    },
    name: {
        type: String,
        required: true
    },
    type: {
        type: String,
        required: true
    },
    category: {
        type: String,
        required: true
    },
    unitPrice: {
        type: Number,
        required: true
    },
    reOrderLevel: {
        type: Number,
        required: true
    },
    dangerLevel: {
        type: Number,
        required: true
    },
    remarks: {
        type: String,
        required: true
    },
    quantity: {
        type: Number,
        required: true
    },
    status:{
      type:Boolean,
      required:true
    }
});

const Drug = mongoose.model('Drug', DrugSchema);

module.exports = Drug;
