'use strict';

const mongoose = require('mongoose');
const Schema = mongoose.Schema;


const PharmacistSchema = new Schema({

    PharmID:{  //username
        type:String,
        unique: true,
        required:true
    },
    name:{
        type:String,
        required:true
    },
    nic:{
        type:String,
        required:true
    },

    gender:{
      type:String,
      required:true
    },

    email:{
      type:String,
      required:true
    },

    cNumber: {
        type: String,
        required: true
    },
    address:{
      type:String,
      required:true
    },
    password:{   //password
      type:String,
      required:true
    }
});


const Pharmacist = mongoose.model('Pharmacist', PharmacistSchema);

module.exports = Pharmacist;
