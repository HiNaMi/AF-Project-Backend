/**
 * Created by kushali on 6/27/2017.
 */
var mongoose = require('mongoose');

module.exports = mongoose.model('Users', {

    username:{
        type:String,
        required:true
    },
    password:{
        type:String,
        required:true
    },
    name:{
        type:String,
        required:true
    }
});