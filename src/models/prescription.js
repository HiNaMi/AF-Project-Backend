/**
 * Created by kushali on 6/27/2017.
 */
var mongoose = require('mongoose');

module.exports = mongoose.model('Prescriptions', {

    presID :{
        type: Number,
        unique: true,
        required: true
    },
    prescribedDate:{
        type:Date,
        required:true
    },
    createdDate: {
        type:Date,
        required:true
    },
    isDespenced:{
        type:boolean,
        required:true
    }

});