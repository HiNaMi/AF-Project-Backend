'use strict'

const mongoose = require('mongoose');

const Schema = mongoose.Schema;

const requestSchema = new Schema({

    requestNo:{
      type:String,
      unique:true,
      required:true
    },
    drugList:[{
      drugID:{type:String},
      quantity:{type:Number}
}],
    department:{
      type:String,
      required:true
    }
});

const SendRequest = mongoose.model('SendRequest', requestSchema);

module.exports = SendRequest;
