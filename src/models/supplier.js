/*var mongoose = require('mongoose');

module.exports = mongoose.model('Supplier', {
    supplierID: {
        type: String,
        unique: true,
        required: true
    },
    name: {
        type: String,
        required: true
    },
    type: {
        type: String,
        required: true
    },
    email: {
        type: String,
        required: true
    },
    cNumber: {
        type: String,
        required: true
    },
    remark: {
        type: Number,
        required: true
    }

});*/

'use strict';

const mongoose = require('mongoose');

const Schema = mongoose.Schema;

const SupplierSchema = new Schema({
  supplierID: {
      type: String,
      unique: true,
      required: true
  },
  name: {
      type: String,
      required: true
  },
  type: {
      type: String,
      required: true
  },
  email: {
      type: String,
      required: true
  },
  cNumber: {
      type: String,
      required: true
  },
  remark: {
      type: String,
      required: true
  }
});

const Supplier = mongoose.model('Supplier', SupplierSchema);

module.exports = Supplier;
