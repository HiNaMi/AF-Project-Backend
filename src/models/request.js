/**
 * Created by kushali on 6/27/2017.
 */

var mongoose = require('mongoose');

module.exports = mongoose.model('Requests', {

    requestID :{
        type: String,
        unique: true,
        required: true
    },
    to:{
        type:String,
        required:true
    },
    From: {
        type:String,
        required:true
    },
    dateTime:{
        type:Date,
        required:true
    },
    description:{
        type:String,
        required:true
    },
    status:{
        type:String,
        required:true
    }

});