'use strict'

const mongoose = require('mongoose');

const Schema = mongoose.Schema;

const billSchema = new Schema({
    billID: {
        type: String,
        unique: true,
        required: true
    },
    drugs:[ {
      drugName  :{type: String,},
      quantity:{type:Number},
      unitPrice:{type:Number},
      billamount: {type: Number}
    }],
    total: {
        type: Number,
        required: true
    }

});

const Bill = mongoose.model('Bill', billSchema);

module.exports = Bill;
