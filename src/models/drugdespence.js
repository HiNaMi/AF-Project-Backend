'use strict'

const mongoose = require('mongoose');

const Schema = mongoose.Schema;

const despenceSchema = new Schema({

    prescriptionNo:{
      type:String,
      unique:true,
      required:true
    },
    patientID:{
      type:String,
      required:true
    },
    drugs:[{
      drugName:{type:String},
      dosage:{type:Number},
      frequency:{type:Number},
      period:{type:Number}
    }],

});

const DrugDespence = mongoose.model('DrugDespence', despenceSchema);

module.exports = DrugDespence;
