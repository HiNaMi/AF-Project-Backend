'use strict'

const mongoose = require('mongoose');

const Schema = mongoose.Schema;

const notificaionSchema = new Schema({
  requestNo:{
    type:String,
    unique:true,
    required:true
  },
  description:{
    type:String,
    required:true
  }
});

const Notification = mongoose.model('Notification', notificaionSchema);

module.exports = Notification;
