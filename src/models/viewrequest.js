'use strict'

const mongoose = require('mongoose');

const Schema = mongoose.Schema;

const viewrequestSchema = new Schema({

    requestNo:{
      type:String,
      unique:true,
      required:true
    },
    drugList:[{
      drugID:{type:String},
      quantity:{type:Number}
    }],
    department:{
      type:String,
      required:true
    },
    isAccepted:{
      type:Boolean,
      required:true
    }
});

const viewRequest = mongoose.model('viewRequest', viewrequestSchema);

module.exports = viewRequest;
