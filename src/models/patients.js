// var mongoose = require('mongoose');
//
// module.exports = mongoose.model('Patients', {
//     patientID: {
//         type: String,
//         unique: true,
//         required: true
//     },
//     title: {
//         type: String,
//         required: true
//     },
//     firstname: {
//         type: String,
//         required: true
//     },
//     lastname: {
//         type: String,
//         required: true
//     },
//     dateofbirth: {
//         type: Date,
//         required: true
//     },
//     gender: {
//         type: String
//     },
//     NIC: {
//         type: String,
//         required: true
//     },
//     address: {
//         type: String,
//         required: true
//     },
//     phoneNo: {
//         type: String,
//         required: true
//     }
// });

'use strict';

const mongoose = require('mongoose');

const Schema = mongoose.Schema;

const PatientSchema = new Schema({
    patientID: {
        type: Number,
        default: 0
    },
    title: {
        type: String,
        required: true
    },
    firstname: {
        type: String,
        required: true
    },
    lastname: {
        type: String,
        required: true
    },
    dateofbirth: {
        type: Date
    },
    gender: {
        type: String
    },
    NIC: {
        type: String,
        required: true
    },
    address: {
        type: String,
        required: true
    },
    phoneNo: {
        type: String,
        required: true
    }
});

const Patient = mongoose.model('Patient', PatientSchema);

module.exports = Patient;
